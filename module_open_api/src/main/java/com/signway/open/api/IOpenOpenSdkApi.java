package com.signway.open.api;

import android.content.Context;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-7-3 : Create IOpenOpenSdkApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenOpenSdkApi {

    /**
     * 连接opensdk，注册eventbus
     * @param context
     */
    void openOpenSdkApi(Context context);

    /**
     * 断开opensdk，取消注册
     * @param context
     */
    void closeOpenSdkApi(Context context);

    /**
     * opensdk是否连接
     */
    boolean isOpenSdkApiConnect();
}
