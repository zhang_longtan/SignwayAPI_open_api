package com.signway.open.api.impl;

import android.content.Context;

import com.signway.open.api.IOpenApiService;

/**
 * Author: ltz
 * Email: lt.zhang@signway.cn
 * Date: 2022/1/8 17:46
 * <p>
 * Description:
 */
public abstract class BaseOpenApiService implements IOpenApiService {
    public boolean needStart = false;

    public boolean isNeedStart(String boradName, Context context) {
        needStart = getModuleName().equals(boradName);
        return needStart;
    }

}
