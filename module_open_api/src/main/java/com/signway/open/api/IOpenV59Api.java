package com.signway.open.api;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-7-2 : Create IOpenV59Api.java
 * -----------------------------------------------------------------
 */
public interface IOpenV59Api extends IOpenControllerApi{
}
