package com.signway.open.api.constant;


/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2018, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: Droid
 *
 * Author: Administrator
 *
 * Email: ##.##@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2018/8/6 0006 :
 * -----------------------------------------------------------------
 */

public class ChangeOrientationConstant {

    public static class Orientation {

        final public static int LANDSCAPE = android.content.pm.ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;

        final public static int PORTRAIT = android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        final public static int SENSOR = android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR;

        final public static int REVERSE_LANDSCAPE = android.content.pm.ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;

        final public static int REVERSE_PORTRAIT = android.content.pm.ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
    }

}
