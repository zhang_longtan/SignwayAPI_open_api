package com.signway.open.api;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-7-2 : Create IOpenControllerApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenControllerApi {
    /**
     * 打开与通讯串口
     */
    void openUart();

    /**
     * 设置GPIO输出
     * @param: gpioGroup: gpio组， gpioNum：gpio号，value：1：高，0：低
     * @return: true: 成功, false: 失败
     */
    boolean gpioPinOutput(int gpioGroup, int  gpioNum, int value);


    /**
     *获取GPIO输入
     * @param: gpioGroup: gpio组， gpioNum：gpio号
     * @return: 1：高，0：低
     */
    int getGpioInput(int gpioGroup, int  gpioNum);
}
