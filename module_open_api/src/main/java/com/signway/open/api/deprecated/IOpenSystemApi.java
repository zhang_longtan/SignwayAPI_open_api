package com.signway.open.api.deprecated;

/**
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 * <p>
 * ProjectName: droid
 * <p>
 * Author: sdl
 * <p>
 * Email: dl.shi@Signway.cn
 * <p>
 * Description:系统操作相关Api
 * <p>
 * -----------------------------------------------------------------
 * 2019/4/2 : Create IOpenSystemApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenSystemApi {

    //定时开关机

    /**
     * @param timerEnable 设置定时开关机状态
     */
    void setSysTimerEnable(int timerEnable);

    /**
     * @return 获取定时开关功能是否开启
     */
    int getSysTimerEnable();

    /**
     * 设置定时关机时间点
     */
    void setSysPowerOffTime(int year, int month, int day, int hourOfDay, int minute);

    /**
     * 设置定时开机时间点
     */
    void setSysPowerOnTime(int year, int month, int day, int hourOfDay, int minute);

    /**
     * @return 获取已设置的开机时间
     */
    String[] getSysPowerOnTime();

    /**
     * @return 获取已设置的关机时间
     */
    String[] getSysPowerOffTime();

    /**
     * @param autoTimeEnable 1：使能自动校准时间；
     *                       0：关闭自动校准时间
     */
    void setAutoTimeEnable(boolean autoTimeEnable);

    /**
     * 设置自动校准时区
     * 1：使能自动校准时区
     * 0：关闭自动校准时区
     */
    int setAutoTimeZoneEnable(boolean enable);

    /**
     * 1：使能自动校准时间
     * 0：关闭自动校准时间
     * -2: 主板不支持此接口
     */
    int getAutoTimeStatus();


    //其他功能

    /**
     * build.prop内属性名称
     *
     * @return 0: 成功， -1: 失败，-2: 主板不支持此接口
     */
    int setSystemProperties(String key, String keyValue);

    /**
     * @param key 属性key
     * @return error:UNKNOWN
     */
    String getSystemProperties(String key);

    /**
     * 关机-睡眠-关屏
     */
    void systemShutDown();

    /**
     * 重启
     */
    void systemReboot();

    /**
     * 关机-低功耗状态
     */
    void systemDeepStandby(int mode, int time);

    /**
     * 系统截屏
     */
    void takeScreenshot(String path, int screenId);

    /**
     * 设置屏幕旋转角度
     *
     * @param rotation 0|90|180|270
     */
    void setRotationScreen(int rotation);


    /**
     * @return 获取屏幕旋转角度
     */
    int getRotationScreen();

    /**
     * 显示或隐藏导航栏
     *
     * @param hide true:隐藏转态栏
     */
    void systemtHideStatusBar(boolean hide);


    /**
     * @return 获取SD卡路劲
     */
    String getSysSDcardPath();

    /**
     * @param num 0-1-2 多个usb口
     * @return 获取usb存储路劲
     */
    String getSysUSBStoragePath(int num);

    /**
     * 格式化数据
     */
    int formatStorage(String path);


    /**
     * @return 2018年7月12日14时52分
     */
    int[] getSystemTime();

    /**
     * 设置系统时间
     */
    void setSystemTime(int year, int month, int day, int hour, int minute);

    /**
     * 喂狗功能
     * 设置：1：打开，0：关闭
     * 返回: 0: 成功， -1: 失败
     */
    int setWatchdogEnable(boolean enable);


    /**
     * 设置喂狗参数
     *
     * @param timeout 超时重启系统 单位：分钟
     * @param count   单位时间内喂狗次数
     */
    int setWatchdogParameters(int timeout, int count);

    /**
     * 喂狗
     */
    int feedWatchdog();

    /**
     * 获取喂狗状态
     */
    int getWatchdogStatus();

}
