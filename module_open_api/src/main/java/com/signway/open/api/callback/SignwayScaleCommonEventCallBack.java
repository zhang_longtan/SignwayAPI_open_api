package com.signway.open.api.callback;

public interface SignwayScaleCommonEventCallBack {
    public void onScaleNotify(int actionId, String sVal, int[] iArray, int intArrayLen);
}
