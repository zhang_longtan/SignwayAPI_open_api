package com.signway.open.api;

import android.content.Context;

/**
 * Author: ltz
 * Email: lt.zhang@signway.cn
 * Date: 2022/1/8 17:41
 * <p>
 * Description:
 */
public interface IOpenApiService {
    boolean isNeedStart(String boradName, Context context);

    String getModuleName();

    IOpenAuthorizedApi getAuthorizedApi();

    IOpenCloudApi getCloudApi();

    IOpenControllerApi getControllerApi();

    IOpenDeviceApi getDeviceApi();

    IOpenDisplayApi getDisplayApi();

    IOpenMcuApi getMcuApi();

    IOpenN68661Api getN68661Api();

    IOpenNetworkApi getNetworkApi();

    IOpenOpenSdkApi getOpenSdkApi();

    IOpenPowerApi getPowerApi();

    IOpenSystemApi getSystemApi();

    IOpenUpdateApi getUpdateApi();

    IOpenV59Api getV59Api();
}
