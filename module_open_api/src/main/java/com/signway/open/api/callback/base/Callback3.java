package com.signway.open.api.callback.base;

/**
 * msg
 * author zx
 * version 1.0
 * since 2018/9/13  .
 */
public abstract class Callback3<X, Y, Z> extends NetCallback {

    public abstract void onMsg(X msg1, Y msg2, Z msg3);

}
