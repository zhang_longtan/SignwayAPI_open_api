package com.signway.open.api;

import android.content.Context;

import com.signway.open.api.callback.base.Callback1;
import com.signway.open.api.callback.base.Callback2;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-7-2 : Create IOpenN68661Api.java
 * -----------------------------------------------------------------
 */
public interface IOpenN68661Api extends IOpenControllerApi{
    /**
     * 打开与n68661通讯串口
     */
//    void openUart();

    /**
     * 参数控制
     */
    void Droid_uart_ioctrl(byte cptr[], int Cmd, int Aux[], byte pBuffer[], int offset);

    /**
     * 获取N68661版本号
     */
    void getN68661Version(Callback1<Integer> callback1);

    /**
     * 升级N6681
     */
    void checkUpdateN68661(Callback2<Boolean, String> callback1);

    /**
     * 升级N6681
     */
    void startUpdateN68661(Context context, String updateFilePath);

}
