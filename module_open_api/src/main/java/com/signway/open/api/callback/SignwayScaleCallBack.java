package com.signway.open.api.callback;

public interface SignwayScaleCallBack {
    public void onScaleNotify(int action, byte[] bytes, int length);
}
