package com.signway.open.api;

import android.content.Context;

import java.io.IOException;


/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-5-29 : Create SignwaySystemApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenSystemApi {


    /**
     * 获取固件build.prop里的信息
     *
     * @param key
     * @return
     */
    String getSystemProperties(String key);

    /**
     * @param key
     * @return
     */
    int getIntSystemProperties(String key);

    /**
     * 设置固件build.prop里的信息
     *
     * @param key
     * @param value
     */
    void setSystemProperties(String key, String value);

    /**
     * @return: API版本号
     */
    String getApiJarVersion();

    /**
     *kernel版本
     * @return: 版本号
     */
    String getKernelVersion();

    /**
     * 设备芯片型号
     * @return
     */
    String getAndroidModel();

    /**
     * 获取固件版本信息  主页面显示
     *
     * @return
     */
    String getAndroidVersion();

    /**
     * 获取固件版本信息  上传服务器
     *
     * @return
     */
    String getFirmwareVer();

    /**
     * 固件升级时， 获取固件版本信息
     *
     * @return
     */
    int getSystemVersion();

    /*
     * 获取板卡信息
     * *** 板卡型号 0:5159  1:5161  2:5166 3:5168 4:5180 5:A20 6:others
     */
    String getBoard();

    /**
     * 读取固件参数，判断是否是上电开机
     * true:第一次上电  false:已经开机
     *
     * @return
     */
    boolean getPowerUp();

    /**
     * 设置上电已经开机
     */
    void setPowerUp();

    /**
     * 读取固件参数，判断是否启动TestAPP
     * true:需要  false:不需要
     *
     * @return
     */
    boolean getIsOpenTestApp();

    /**
     * 设置系统喂狗
     * apk启动，先关闭系统喂狗，再打开apk喂狗
     * apk退出，先关闭apk喂狗，再打开系统喂狗
     * set  true：打开    false：关闭
     * <p>
     * sys.firmware.watchdog  enable disable exit
     */
    void setSystemWatchDog(boolean set);


    /**
     * 获取喂狗开关
     * @return: enable: 0: 关闭喂狗功能, 1: 打开喂狗功能;
     */
    int getWatchdogOnoff();


    /**
     * 设置喂狗超时时间
     * @param: timeout: 最低30s，默认3分钟
     * @return: true: 成功, false: 失败
     */
    boolean setWatchdogTimeout(int timeout);

    /**
     * 开始喂狗
     */
    void startWatchDog();

    /**
     * 停止喂狗
     */
    void stopWatchDog();

    /**
     * 关闭喂狗
     */
    void feedWatchDog();

    /**
     * 获取屏幕当前旋转角度
     *
     * @return
     */
    int getOrientation();

    /**
     * 设置屏幕旋转角度
     *
     * @param orientation
     * @param context
     */
    void setOrientation(int orientation, Context context);

    /**
     * 获取sn号
     *
     * @return
     */
    String getSerial();

    /**
     * @return
     */
    boolean isReadSerial();

    /**
     * 截屏
     *
     * @param filename
     * @return
     */
    boolean screenShot(String filename);

    /**
     * 截屏
     *
     * @param filename  截屏文件存储文件名
     * @param screenType  屏幕类型
     * @return
     */
    boolean screenShot(String filename, int screenType);

    /**
     * 获取系统下面的导航栏 true为显示。false为隐藏
     *
     */
    boolean getSystemNavigationStatus( Context context);

    /**
     * 隐藏或者显示系统下面的导航栏 true为显示。false为隐藏
     *
     * @param flag
     */
    void operateSystemNavigation(boolean flag, Context context);

    /**
     * 获取运行内存容量
     * @return: 内存DDR大小,单位GB; 例如: "4.0GB"
     */
    String getRunningMemory();

    /**
     * 获取Scaler版本
     * @return
     */
    String getScalerVersion();

    /**
     * 获取内部存储容量
     * @return: 存储大小,单位GB; 例如: "8.0GB"
     */
    float getInternalStorageMemory();

    /**
     * 调用adb命令
     * @param rebootCmd
     * @throws IOException
     */
    void execRootCommand(String rebootCmd) throws IOException;

    /**
     * 格式化
     * @param mount_type
     * @param context
     * @return
     */
    boolean format(int mount_type, final Context context);

    /**
     * 设置系统时间
     * @param time
     */
    void setSystemTime(long time);

    /**
     * 设置自动同步系统时间
     * @param isAuto
     */
    void setSystemTimeAuto(boolean isAuto);

    /**
     * 设置自动同步系统时区
     * @param isAuto
     */
    void setSystemTimezoneAuto(boolean isAuto);


    /**
     *
     *@return: onoff: 0: 关闭自动同步, 1: 打开自动同步
     */
    int getAutoTimeOnoff();

    /**
     *
     *@return: onoff: 0: 关闭自动同步, 1: 打开自动同步
     */
    int getAutoTimeZoneOnoff();

    /**
     * 深度待机 手动模式
     */
    boolean setPowerDeepStandby();


    /**
     * 深度待机
     * @param time
     */
    boolean setPowerDeepStandby(int time);

    /**
     * 获取最后一次点击屏幕的时间
     */
    long getLastTouchTime();


    /**
     * 安卓系统重启
     * @return  true: 成功, false: 失败
     */
    boolean setAndroidReboot();


    /**
     * setDeviceReboot()
     * @return  true: 成功, false: 失败
     */
    boolean setDeviceReboot();

    /**
     *
     * @param: onoff: false: 系统浅度关机, true: 系统浅度开机
     * @return: true: 成功, false: 失败
     */
    boolean setPowerShallowStandby(boolean onoff);


    /**
     *
     * @return: false: 系统浅度关机, true: 系统浅度开机
     */
    boolean getPowerShallowStandby();

    /**
     * @param: enable: 0: 关闭风扇, 1: 打开风扇
     * @return: true: 成功, false: 失败
     */
    boolean setFanEnable(int enable);

    /**
     *
     * @return: status: 0: 关闭风扇, 1: 打开风扇
     */
    int getFanStatus();

    /**
     * @param: path: apk文件路径
     * @return: true: 成功, false: 失败
     */
    boolean silentInstallApk(String path, boolean isAuto);

    /**
     * @param: packageName: apk的包名
     * @return: true: 成功, false: 失败
     */
    boolean silentUninstallApk(String packageName);

    /**
     *
     * @return: true: 成功, false: 失败
     */
    boolean setFormatInternalSD();

    /**
     *
     * @param: power: 1-12w
     *               resistance: 2,4,6,8Ω
     * @return: true: 成功, false: 失败
     */
    boolean setSpeakerParameters(int power, int resistance);

    /**
     *
     * @return: byte[0]:power, byte[1]:resistance
     */
    byte[] getSpeakerParameters();



}
