package com.signway.open.api;

import android.util.Log;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-8-31 : Create IOpenAuthorizedApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenAuthorizedApi {


    /**
     * 授权方式 0:DM2016授权  1:文件授权
     * @return
     */
    int SystemAuthorizationWay();


    /**
     * 设置激活状态
     *
     * @param value
     */
    public void setAuthType(int value);


    /**
     * 获取激活状态
     *
     * @param
     */
    public int getAuthType();


    /**
     * 设置授权总时长
     *
     * @param
     */
    public void setAuthTotalTime(byte[] buffer, int buf_len) ;


    /**
     * 设置授权总时长
     *
     * @param
     */
    public void setAuthTotalTime(long value) ;


    /**
     * 获取授权总时长
     *
     * @param
     */
    public long getAuthTotalTime() ;


    /**
     * 设置授权失效日期
     *
     * @param
     */
    public void setAuthEndDate(byte[] buffer, int buf_len);

    public void setAuthEndDate(String date);

    //获取授权失效日期
    public String getAuthEndDate();

    // 设置授权开始日期
    public void setAuthStartDate(byte[] buffer, int buf_len);

    public void setAuthStartDate(String date);

    // 获取授权失效日期
    public String getAuthStartDate();

    // 设置累计播放时长
    public void setWorkTime(byte[] buffer, int buf_len);

    public void setWorkTime(long value);

    //获取累计播放时长
    public long getWorkTime();



}
