package com.signway.open.api;


import android.content.Context;


/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:网络相关设置接口
 *
 * -----------------------------------------------------------------
 * 2019-5-30 : Create SignwayNetworkApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenNetworkApi {

    /**
     * 以太网开关
     *
     * @param isOpen true:开 false:关
     */
    void setEthernet(boolean isOpen);

    /**
     * wifi开关
     *
     * @param isOpen true:开 false:关
     */
    void setWifi(boolean isOpen);

    /**
     * 手机移动网络开关
     *
     * @param isOpen true:开 false:关
     */
    void setMobile(boolean isOpen);

    /**
     * 获取mac地址
     *
     * @return
     */
    String getMac(int boardType);

    /**
     * 获取静态地址IP
     *
     * @return
     */
    String getDhcpIp();

    /**
     * 获取静态地址掩码
     *
     * @return
     */
    String getDhcpMask();

    /**
     * 获取静态地址网关
     *
     * @return
     */
    String getDhcpGetway();

    /**
     * 获取静态地址域名1
     *
     * @return
     */
    String getDhcpDns1();

    /**
     * 获取网络地址掩码
     *
     * @return
     */
    String getNetworkMask(Context context,int workMode);

    /**
     * 获取当前网络名称   wifi  以太网
     */
    String getNetworkName(Context context);

    /**
     * 设置以太网静态IP
     *
     * @return
     */
    boolean setStaticIpConfiguration(String localIP,String localMask,String localGateway,String localDNS,String localDNS1);

    /**
     * 设置DHCP
     */
    void setDhcpIpConfiguration();

    /**
     * 移动网络修复
     */
    void repairMobile();

    /**
     * 获取wifi状态
     * @return
     */
    int getWifiState();

    /**
     * 获取以太网连接状态
     * @return
     */
    int getEthernetConnectStatus();

    /*
     * 打开网络 mode：NETWORK_ETHERNET、NETWORK_WIFI、NETWORK_3G
     */
    void openNetworkMode(int mode,boolean isMobileOpen);

    /*
     * 初始化wifi
     */
    void initWifi(int workMode);

    /**
     * 本地是否连接
     */
    boolean isLocalConnected(int workMode);
}
