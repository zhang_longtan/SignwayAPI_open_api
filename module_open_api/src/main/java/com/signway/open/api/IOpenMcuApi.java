package com.signway.open.api;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description: 微控制单元mcu   v59    n68661
 *
 * -----------------------------------------------------------------
 * 2019-6-27 : Create IOpenMcuApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenMcuApi extends IOpenControllerApi{

    /**
     * 打开与n68661通讯串口
     */
//    void openUart();

    /**
     * 参数控制
     */
    void Droid_uart_ioctrl(byte cptr[], int Cmd, int Aux[], byte pBuffer[], int offset);

    /**
     * 获取Mcu版本
     * @return
     */
    String getMcuVersion();

}
