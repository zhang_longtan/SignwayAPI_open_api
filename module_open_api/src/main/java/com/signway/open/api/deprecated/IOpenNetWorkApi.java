package com.signway.open.api.deprecated;

/**
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 * <p>
 * ProjectName: droid
 * <p>
 * Author: sdl
 * <p>
 * Email: dl.shi@Signway.cn
 * <p>
 * Description:网络相关操作
 * <p>
 * -----------------------------------------------------------------
 * 2019/3/28 : Create IOpenNetWorkApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenNetWorkApi {

//    /**
//     * 获取当前网络类型
//     *
//     * @return 100 ->无网络连接；0-->以太网；1-->WIFI；2-->2G网络；3-->3G或3G以上的网络
//     */
//    int getCurrentNetType();

    //以太网相关

    /**
     * @return 获取以太网的MAC地址
     */
    String getEthMacAddress();

    /**
     * 设置以太网MAC地址
     */
    void setEthMacAddress(String mac);

    /**
     * 获取当前以太网模式
     * <p>
     * Staticip-->静态  DHCP-->动态
     *
     * @return 1:静态 2.动态
     */
    int getEthMode();

    /**
     * DHCP: 动态IP
     * Staticip: 静态IP
     *
     * @param mode 1:静态 2.动态
     */
    void setEthMode(int mode);

    /**
     * 设置以太网静态IP
     * <p>
     * String iPaddr = "192.168.0.121";
     * String gateWay = "192.168.0.1";
     * String mask = "255.255.255.0";
     * String dns1 = "8.8.8.8";
     * String dns2 = "0.0.0.0";
     */
    void setEthStaticIPAddress(String ip, String gw, String mask, String dns1, String dns2);

    /**
     * @return 获取以太网静态IP
     */
    String getEthStaticIPAddress();

    /**
     * @return 获取以太网动态IP
     */
    String getEthIpAddress();

    /**
     * 以太网开关
     *
     * @param open true:开 false:关
     */
    void seEthEnabled(boolean open);


    //4G模块复位
    int set4GModuleReset();


    //wifi

    /**
     * wifi开关
     *
     * @param open true:开 false:关
     */
    void setWifiEnabled(boolean open);

    /**
     * wifi动态连接
     */
    void setWifiDHCP();

    /**
     * wifi静态连接
     */
    void setWifiStatic(String ip, String gw, String mask, String dns);

    /**
     * @return 1 静态 0 动态 -1 未知
     */
    int wifiIsStatic();

    /**
     * 网络是否连接，自动检测当前使用的网络
     *
     * @return true:连接 false 未连接
     */
    boolean netIsConnected();


    //移动网络

    /**
     * 移动网络开关
     *
     * @param open true:开 false:关
     */
    void setMobileEnabled(boolean open);


    //网络信息

    /**
     * 获取子网掩码
     *
     * @param type 1:以太网 2：wifi
     *             * ETHERNET ：以太网掩码
     *             * WIFI ：WiFi掩码
     */
    String getNetMask(int type);

    /**
     * 获取网关
     */
    String getNetGateway(int type);

    /**
     * 获取DNS
     */
    String getNetDns(int type);

    /**
     * 获取IP地址
     */
    String getNetIpAddress(int type);
}
