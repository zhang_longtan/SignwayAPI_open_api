package com.signway.open.api.deprecated;

/**
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 * <p>
 * ProjectName: droid
 * <p>
 * Author: sdl
 * <p>
 * Email: dl.shi@Signway.cn
 * <p>
 * Description: 获取硬件相关信息
 * <p>
 * -----------------------------------------------------------------
 * 2019/4/2 : Create IOpenHardwareInfoApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenHardwareInfoApi {
    /**
     * @return 获取api版本号
     */
    String getApiVersion();

    /**
     * @return 获取Android版本号
     */
    String getAndroidVersion();

    /**
     * @return 获取固件版本号
     */
    String getFirmwareVersion();

    /**
     * @return 获取固件编译日期
     */
    String getFirmwareDate();

    /**
     * @return 获取内核版本号
     */
    String getKernelVersion();

    /**
     * @return 获取系统版本和编译日期
     */
    String getAndroidDisplay();

    /**
     * @return 获取运行内存容量
     */
    String getRunningMemory();

    /**
     * @return 获取内部存储容量
     */
    String getInternalStorageMemory();


}
