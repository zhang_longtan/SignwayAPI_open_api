package com.signway.open.api;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-5-29 : Create SignwayDeviceApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenDeviceApi {

    /**
     * 开启拼接功能串口通讯
     */
    void OpenJoint();

    /**
     *打开串口
     */
    int openUart(String device, int baud);

    /**
     * 关闭串口
     */
    int closeUart(int fd);

    /**
     * 从串口读数据
     */
    int readUart(int fd, byte[] buf, int len);

    /**
     * 往串口写数据
     */
    int writeUart(int fd, byte[] buf, int len);

    /**
     * 读eeprom
     */
    int ReadEepromInfo(int pos, byte[] buff, int len);

    /**
     * 写eeprom
     */
    int WriteEepromInfo(int pos, byte[] buff, int len);

    /**
     * 设置激活类型
     *
     * @param value
     */
    void setAuthType(int value);

    /**
     * 获取激活类型
     */
    int getAuthType();

    /**
     * 设置工作总时长
     *
     * @param value
     */
    void setWorkTime(long value);

    /**
     * 获取工作总时长
     */
    long getWorkTime();

    /**
     * 设置授权总时长
     *
     *
     * @param
     */
    void setAuthTotalTime(long value);

    /**
     * 获取授权总时长
     */
    long getAuthTotalTime();

    /**
     * 设置授权结束日期
     *
     * @param date
     */
    void setAuthEndDate(String date);

    /**
     * 获取授权结束日期
     */
    String getAuthEndDate();

    /**
     * 设置授权开始日期
     */
    void setAuthStartDate(String date);

    /**
     * 获取授权开始日志
     */
    String getAuthStartDate();

    /**
     * 设置SN
     * @return
     */
    void setSN(String sn);

    /**
     * 获取SN
     * @return
     */
    String getSN();

    /**
     * 设置MAC
     * @return
     */
    void setMAC(String mac);

    /**
     * 获取MAC
     * @return
     */
    String getMAC();

    /**
     * 设置屏幕角度
     * @param: id: "LVDS","LVDS2","HDMI","eDP","MIPI","VB1"
     *                rotation: 0,90,180,270; 逆时针旋转
     * @return: true: 成功, false: 失败
     */
    boolean setScreenRotation(int id, int rotation);

    /**
     * 获取屏幕角度
     * @param: id: "LVDS","LVDS2","HDMI","eDP","MIPI","VB1"
     * @return: rotation
     */
    int getScreenRotation(int id);
}
