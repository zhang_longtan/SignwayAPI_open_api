package com.signway.open.api.constant;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2018, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: Droid
 *
 * Author: Administrator
 *
 * Email: ##.##@Signway.cn
 *
 * Description:设置的常量类
 *
 * -----------------------------------------------------------------
 * 2018/8/6 0006 :
 * -----------------------------------------------------------------
 */

public class SettingConstant {

    /*** 工作模式 ****/
    /**
     * 单机
     **/
    public static final int SINGLE = 0;
    /**
     * 有线
     **/
    public static final int NETWORK = 1;
    /**
     * WIFI
     **/
    public static final int WIFI = 2;
    /**
     * 移动网络
     **/
    public static final int MOBILE = 3;

    /*** 存储器优先 ***/
    /**
     * 内置存储器
     **/
    public static final int STORAGE_INLAY = 0;
    /**
     * U盘
     **/
    public static final int STORAGE_UDISK = 1;
    /**
     * sd卡
     **/
    public static final int STORAGE_SDDISK = 2;


}
