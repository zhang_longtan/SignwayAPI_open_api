package com.signway.open.api.callback.base;


/**
 * 父类接口回调
 * author zx
 * version 1.0
 * since 2018/10/23  .
 */
public abstract class NetCallback extends BaseCallback {

    public void onNetwork(boolean net) {

    }

    public void onTimeOut() {

    }

}
