package com.signway.open.api.deprecated;

/**
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 * <p>
 * ProjectName: droid
 * <p>
 * Author: sdl
 * <p>
 * Email: dl.shi@Signway.cn
 * <p>
 * Description:升级相关功能
 * <p>
 * -----------------------------------------------------------------
 * 2019/4/2 : Create IOpenUpdateApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenUpdateApi {

    /**
     * 静默安装Apk
     *
     * @param path        文件路劲
     * @param packageName 包名
     * @param mode        1：启动， 0：不启动
     * @return true:安装成功
     */
    boolean silentInstallApk(String path, String packageName, int mode);

    /**
     * 静默卸载Apk
     *
     * @param packageName 包名
     */
    int silentUninstallApk(String packageName);

    /**
     * OTA 系统升级
     */
    void upgradeSystem(String path);

}
