package com.signway.open.api;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-5-29 : Create SignwayDisplayApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenDisplayApi {

    /**
     * 开关背光
     * @param isEnabled
     */
    void setBackLightEnable(boolean isEnabled);

    /**
     * 设置亮度
     * @param value
     */
    void setBrightness(int value);

    /**
     * 对比度
     *
     * @param value
     */
    void setContrast(int value);

    /**
     * 设置颜色
     * @param value
     */
    void setColor(int value);

    /**
     * 设置色调
     * @param value
     */
    void setTint(int value);


    /**
     * 设置尖锐度
     * @param value
     */
    void setSharpness(int value);

    /**
     * 获取LVDS 分辨率
     * @param value
     */
    String getLvdsResolution(int value);

    /**
     * 设置LVDS 分辨率
     * @param value
     */
    void setLvdsResolution(int value);

    /**
     * 设置屏幕调光方向
     * @param screen
     * @param status
     */
    void setBacklightInverse(int screen, int status);

    /**
     * 获取屏幕调光方向
     * @param id
     * @return
     */
    int getScreenBacklightInverse(int id);

    /**
     *设置屏幕调光方式
     * @param screen
     * @param mode
     */
    void setBacklightMode(int screen, int mode);


    /**
     * 获取屏幕调光方式
     * @param id
     * @return
     */
    int getScreenBacklightMode(int id);

    /**
     *设置屏幕背光亮度
     * @param id
     * @param brightness
     */
    void setScreenBrightness(int id, int brightness);


    /**
     * 获取屏幕背光亮度
     * @param id
     * @return
     */
    int getScreenBrightness(int id);


    /**
     * 设置屏幕开关
     * @param: id: "LVDS","LVDS2","eDP","MIPI","VB1"
     *                onoff: true:打开，false：关闭
     * @return: true: 成功, false: 失败
     */
    boolean setScreenOnoff(int id, boolean onoff);


    /**
     * 获取屏幕状态
     * @param: id: "LVDS","LVDS2","eDP","MIPI","VB1"
     * @return: true：打开，false：关闭
     */
    boolean getScreenStatus(int id);

    /**
     * 设置屏幕分辨率
     * @param: id: "ALL","LVDS","LVDS2","eDP","MIPI","VB1"
     *                resolution: enum枚举例如：“1920x1080P50”
     * @return: true: 成功, false: 失败
     */
    boolean setScreenResolution(int id, String resolution);


    /**
     *获取屏幕分辨率
     * @param: id: "ALL","LVDS","LVDS2","eDP","MIPI","VB1"
     * @return: resolution: enum枚举例如：“1920x1080P50”
     */
    String getScreenResolution(int id);


    /**
     * 设置使能HDMI开关
     * @param: enable: 0: hdmi关, 1: hdmi开
     * @return: true: 成功, false: 失败
     */
    boolean setHdmiOnoff(boolean onoff);


    /**
     * 获取HDMI使能状态
     * @return: status: false: hdmi关, true: hdmi开
     */
    boolean getHdmiOnoff();

    /**
     * 设置HDMI分辨率
     * @param: resolution: 例如: "1920x1080@60P"
     * @return: true: 成功, false: 失败
     */
    boolean setHdmiResolution(int resolution);


    /**
     * 获取HDMI分辨率
     * @return: resolution: 例如: "1920x1080@60P"
     */
    String getHdmiResolution();

    /**
     *设置Lvds模式
     * @param: id: "LVDS","LVDS2"
     *                mode:   0: vesa, 1: jeida
     * @return: true: 成功, false: 失败
     */
    boolean setLvdsMode(String id, int mode);


    /**
     * 获取Lvds模式
     * @param: id: "LVDS","LVDS2"
     * @return: mode:   0: vesa, 1: jeida
     */
    int getLvdsMode(String id);


    /**
     * Lvds奇偶交换 设置奇偶交换
     * @param: id: "LVDS","LVDS2"
     *                swap:  0: noswap, 1: swap
     * @return: true: 成功, false: 失败
     */
    boolean setLvdsSwap(String id, int swap);


    /**
     *获取奇偶交换
     * @param: id: "LVDS","LVDS2"
     * @return: swap:  0: noswap, 1: swap
     */
    int getLvdsSwap(String id);

    /**
     *设置Lvds通道
     * @param: screen: id: "LVDS","LVDS2"
     * 	   channel:  0: signal, 1: dual
     * @return: true: 成功, false: 失败
     */
    boolean setLvdsChannel(int screen, int channel);


    /**
     *获取Lvds通道
     * @param: screen: id: "LVDS","LVDS2"
     * @return: channel:  0: signal, 1: dual
     */
    int getLvdsChannel(int screen);



}
