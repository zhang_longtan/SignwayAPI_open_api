package com.signway.open.api;

import android.content.Context;

import com.signway.open.api.callback.base.Callback1;
import com.signway.open.api.callback.base.Callback2;

/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-5-30 : Create SignwayUpdateApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenUpdateApi {

    /**
     * 安装apk
     * @param path
     */
    void installAPKSilent(String path);

    /**
     * 静默安装
     * @param path
     * @param action
     */
    void installAPKSilent(String path, String action);

    /**
     * 卸载
     * @param context
     * @param packageName
     */
    void uninstallAPK(Context context, String packageName);

    /**
     * 设置系统时间
     * @param current
     */
    void setSystemCurrentTimeMillis(long current);

    /**
     * 固件ota升级
     * @param filePath
     */
    void updateOTA(String filePath);

}
