package com.signway.open.api;


/*
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 *
 * ProjectName: droid5119
 *
 * Author: Administrator
 *
 * Email: pf.wang@Signway.cn
 *
 * Description:
 *
 * -----------------------------------------------------------------
 * 2019-5-29 : Create SignwayPowerApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenPowerApi {


    /**
     * 重启
     */
    void reboot();

    /**
     * 设置下次开机时间
     * @param millis
     */
    void setPowerOnTime(long millis);

    /**
     *关机
     */
    void powerOff();

    /**
     * 休眠
     */
    void shutDown();

}
