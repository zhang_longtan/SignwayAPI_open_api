package com.signway.open.api.deprecated;

/**
 * -----------------------------------------------------------------
 * Copyright (C) 2011-2019, by your Signway, All rights reserved.
 * -----------------------------------------------------------------
 * <p>
 * ProjectName: droid
 * <p>
 * Author: sdl
 * <p>
 * Email: dl.shi@Signway.cn
 * <p>
 * Description:硬件io口 读写操作
 * <p>
 * -----------------------------------------------------------------
 * 2019/3/28 : Create IOpenIoPortApi.java
 * -----------------------------------------------------------------
 */
public interface IOpenIoPortApi {

    //串口

    /**
     * 打开一个串口
     *
     * @param deviceName 串口设备名称 eg./dev/ttyS0
     * @param baud       波特率 eg.115200
     * @return 返回串口ID
     */
    int serialDeviceOpen(String deviceName, int baud);

    /**
     * 关闭串口
     *
     * @param fd 串口id
     * @return 1:成功 0：失败
     */
    int serialDeviceClose(int fd);

    /**
     * 串口读取
     *
     * @param fd     串口id
     * @param buffer 读取的数组
     * @param length 读取长度
     * @return 实际读取的长度
     */
    int serialRead(int fd, byte[] buffer, int length);

    /**
     * 串口写
     *
     * @param fd     串口id
     * @param buffer 写入数组
     * @param length 写入长度
     * @return 0:成功 -1：失败
     */
    int serialWrite(int fd, byte[] buffer, int length);

    //i2c

    /**
     * 打开I2C设备
     *
     * @param deviceName 设备名称
     * @return i2c设备id
     */
    int i2cDeviceOpen(String deviceName);

    /**
     * 关闭i2c
     *
     * @param fd 设备id
     */
    int i2cDeviceColse(int fd);

    /**
     * i2c读
     */
    int i2cRead(int fd, int offset, byte[] buffer, int length);

    /**
     * i2c写
     */
    int i2cWrite(int fd, int offset, byte[] buffer, int length);

    //gpio

    /**
     * gpio读
     */
    int gpioDeviceOpen(String deviceName);

    /**
     * gpio关闭
     */
    int gpioDeviceClose(int fd);

    /**
     * @param fd    gpio设备id
     * @param cmd   控制命令
     * @param gpio  gpio脚
     * @param level gpio电平
     * @return 0：成功 -1：失败 -2：不支持此接口
     */
    int gpioPinCtrl(int fd, int cmd, int gpio, int level);
}
