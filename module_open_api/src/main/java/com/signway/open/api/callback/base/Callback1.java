package com.signway.open.api.callback.base;

/**
 * msg
 * author zx
 * version 1.0
 * since 2018/9/13  .
 */
public abstract class Callback1<X> extends NetCallback {

    public abstract void onMsg(X msg);

}
